# Roche Covid-19

## Objective of this repository
- Summarize and versionize the tasks for Roche.
- Illustrate findings of those tasks.
- Deployment of development, testing and master outcome.
- Development and Discussion platform.

## Branches
- development: work in progress
- testing: work in internal review
- master: ready to deploy for Roche

## Table of Contents:

- Task 06.10.2020: 
-- Search vor Covid-19 datasets on testing on us-county level
-- proposed outcome: tabular summary of the data found.
-- folder: dataset_research